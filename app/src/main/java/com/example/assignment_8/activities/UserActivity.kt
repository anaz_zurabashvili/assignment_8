package com.example.assignment_8.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.assignment_8.databinding.ActivityUserBinding
import com.example.assignment_8.extensions.emailValid
import com.example.assignment_8.models.User

class UserActivity : AppCompatActivity() {
    private lateinit var user: User
    private lateinit var binding: ActivityUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        if (intent.extras?.containsKey("user") == true) {
            user = intent.extras?.get("user") as User
            getHints(user)
        }
        binding.btnUserSave.setOnClickListener(saveBtnClick)
    }


    private fun isAllFieldsValid(): Boolean = !(binding.etEmailAddress.text.isNullOrBlank() ||
            binding.etFirstName.text.isNullOrBlank() ||
            binding.etLastName.text.isNullOrBlank())

    private fun makeValidation(): Boolean =    isAllFieldsValid() &&
            binding.etEmailAddress.text.toString().emailValid()

    private fun isPetFieldsValid(): Boolean = !(binding.etPetType.text.isNullOrBlank() ||
            binding.etPetType.text.isNullOrBlank())

    fun getHints(user: User) {
        binding.etFirstName.hint = user.firstName
        binding.etLastName.hint = user.lastName
        binding.etEmailAddress.hint = user.emailAddress
        binding.etPetType.hint = user.pet?.type ?: "Type"
        binding.etPetName.hint = user.pet?.name ?: "Name"
    }

    private val saveBtnClick = View.OnClickListener {
        if (makeValidation()) {
            if ((intent.extras?.get("user")) == null) {
                user = User(
                    binding.etFirstName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etEmailAddress.text.toString(),
                    if (isPetFieldsValid())
                        User.Pet(
                            binding.etPetType.text.toString(),
                            binding.etPetName.text.toString()
                        ) else null
                )

                val intent = intent
                intent.putExtra("user", user)
                setResult(RESULT_OK, intent)
                finish()
            } else {
                user.changeInfo(
                    binding.etFirstName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etEmailAddress.text.toString(),
                    if (isPetFieldsValid())
                        User.Pet(
                            binding.etPetType.text.toString(),
                            binding.etPetName.text.toString()
                        ) else null
                )
                val intent = intent
                intent.putExtra("user", user)
                intent.putExtra("position", intent.extras?.getInt("position"))
                setResult(RESULT_OK, intent)
                finish()
            }
        } else {
            binding.btnUserSave.text = "Try Again!"
        }
    }
}

