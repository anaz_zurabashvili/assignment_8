package com.example.assignment_8.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.GridLayoutManager
import com.example.assignment_8.adapters.UsersAdapter
import com.example.assignment_8.databinding.ActivityUsersBinding
import com.example.assignment_8.models.User

class UsersActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUsersBinding
    private val users = mutableListOf<User>()
    private lateinit var adapter: UsersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUsersBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setData()
        initRecyclerView()
        binding.btnAddUser.setOnClickListener(btnAddUserClick)
    }

    private fun initRecyclerView() {
        binding.recyclerView.layoutManager = GridLayoutManager(this, 2)
        adapter = UsersAdapter()
        binding.recyclerView.adapter = adapter
        adapter.userRemoveCallBack = {
            users.removeAt(it)
        }
        adapter.userUpdateCallBack = {
            onUpdateClick(it)
        }
        adapter.setData(users)
    }

    private val btnAddUserClick = View.OnClickListener {
        val intent = Intent(this, UserActivity::class.java)
        startForResult.launch(intent)
    }
    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                var user = it.data?.extras?.get("user") as User
                users.add(user)

                adapter.notifyItemInserted(users.size - 1)
                adapter.insert(user)

                intent.removeExtra("user")
            }
        }

    private fun onUpdateClick(position: Int) {
        val intent = Intent(this, UserActivity::class.java)
        intent.putExtra("user", users[position])
        intent.putExtra("position", position)
        editStartForResult.launch(intent)
    }

    private val editStartForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                val position = it.data?.extras?.getInt("position")
                d("anuki", "$position")
                val user = it.data?.extras?.get("user") as User
                users[position!!] = user
                adapter.notifyItemChanged(position!!)
                adapter.update(user, position!!)

                intent.removeExtra("user")
                intent.removeExtra("position")

            }
        }

    private fun setData() {
        users.add(User("User 1", "Content 1", "emailAddress"))
        users.add(User("User 2", "Content 2", "emailAddress"))
        users.add(User("User 3", "Content 3", "emailAddress"))
        users.add(User("User 4", "Content 4", "emailAddress", User.Pet("Dog", "Jerry")))
        users.add(User("User 5", "Content 5", "emailAddress"))
        users.add(User("User 6", "Content 6", "emailAddress"))
        users.add(User("User 7", "Content 7", "emailAddress", User.Pet("Cat", "Anya")))
        users.add(User("User 8", "Content 8", "emailAddress", User.Pet("Cat", "Lucy")))
        users.add(User("User 9", "Content 9", "emailAddress"))
    }
}