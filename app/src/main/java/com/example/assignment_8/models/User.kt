package com.example.assignment_8.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    var firstName: String,
    var lastName: String,
    var emailAddress: String,
    var pet: Pet? = null
) : Parcelable {
    @Parcelize
    data class Pet(var type: String, var name: String):Parcelable

    fun changeInfo(
        firstNameChanged: String,
        lastNameChanged: String,
        emailAddressChanged: String,
        petChanged: Pet?
    ) {
        firstName = firstNameChanged
        lastName = lastNameChanged
        emailAddress = emailAddressChanged
        if (pet!=null){
            pet = petChanged
        }
    }
}