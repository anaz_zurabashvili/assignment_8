package com.example.assignment_8.extensions

import android.view.View
import com.google.android.material.snackbar.Snackbar


fun View.showSnackBar(title:String)=
    Snackbar.make(this, title, Snackbar.LENGTH_SHORT).show()