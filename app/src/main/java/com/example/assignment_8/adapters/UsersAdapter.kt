package com.example.assignment_8.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment_8.R
import com.example.assignment_8.databinding.FirstUserLayoutBinding
import com.example.assignment_8.databinding.SecondUserLayoutBinding
import com.example.assignment_8.models.User

typealias UserRemoveClick = (position: Int) -> Unit
typealias UserUpdateCallBack = (position: Int) -> Unit


class UsersAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val FIRST_ITEM = 10
        private const val SECOND_ITEM = 11
    }

    private val users = mutableListOf<User>()
    var userRemoveCallBack: UserRemoveClick? = null
    var userUpdateCallBack: UserUpdateCallBack? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        if (viewType == FIRST_ITEM) {
            FirstViewHolder(
                FirstUserLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            SecondViewHolder(
                SecondUserLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FirstViewHolder) {
            holder.onBind()
        } else if (holder is SecondViewHolder) {
            holder.onBind()
        }
    }

    override fun getItemCount(): Int = users.size

    override fun getItemViewType(position: Int): Int =
        if (users[position].pet != null) SECOND_ITEM else FIRST_ITEM

    inner class FirstViewHolder(private val binding: FirstUserLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private lateinit var model: User

        fun onBind() {
            model = users[adapterPosition]
            binding.tvEmailAddress.text = model.emailAddress
            binding.tvFirstName.text = model.firstName
            binding.tvLastName.text = model.lastName
            binding.ivDelete.setOnClickListener(this)
            binding.ivUpdate.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.ivDelete -> {
                    userRemoveCallBack?.invoke(adapterPosition)
                    delete(adapterPosition)
                }
                R.id.ivUpdate -> {
                    userUpdateCallBack?.invoke(adapterPosition)
                }
            }
        }
    }

    inner class SecondViewHolder(private val binding: SecondUserLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private lateinit var model: User

        fun onBind() {
            model = users[adapterPosition]
            binding.tvEmailAddress.text = model.emailAddress
            binding.tvFirstName.text = model.firstName
            binding.tvLastName.text = model.lastName
            binding.tvPetType.text = model.pet!!.type
            binding.tvPetName.text = model.pet!!.name
            binding.ivDelete.setOnClickListener(this)
            binding.ivUpdate.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.ivDelete -> {
                    userRemoveCallBack?.invoke(adapterPosition)
                    delete(adapterPosition)
                }
                R.id.ivUpdate -> {
                    userUpdateCallBack?.invoke(adapterPosition)
                }
            }
        }
    }

    fun setData(users: MutableList<User>) {
        this.users.clear()
        this.users.addAll(users)
        notifyDataSetChanged()
    }

    fun delete(position: Int) {
        this.users.removeAt(position)
        notifyItemRemoved(position)
    }

    fun update(user: User, position: Int) {
        users[position] = user
        notifyItemChanged(position)
    }

    fun insert(user: User) {
        users.add(user)
        notifyItemInserted(users.size - 1)
    }
}